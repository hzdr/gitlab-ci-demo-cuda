# Sample CUDA project using GitLab CI

[![build status](https://gitlab.hzdr.de/examples/gitlabci-cuda/badges/master/build.svg)](https://gitlab.hzdr.de/examples/gitlabci-cuda/commits/master)

If you have a CUDA project and want to use GPUs for testing, you can do this
with GitLab CI. This project tells you how.

1. To use GitLab CI you need to place a file called `.gitlab-ci.yml` in the
   root-directory of your project.
   
   - NVIDIA provides official CUDA docker images. You can find them here:
     https://hub.docker.com/r/nvidia/cuda/
     
   - a very basic example of a `.gitlab-ci.yml`-file is shown here:
   
     ```yml
     test_cuda:
       # use latest cuda-image
       image: nvidia/cuda:latest
       script:
         - nvidia-smi
         # make samples
         - make -j4
         # change directory to binary location
         - cd bin/x86_64/linux/release
         # execute some random binaries
         - ./deviceQuery
         - CUDA_VISIBLE_DEVICES=0 ./matrixMulCUBLAS
         - CUDA_VISIBLE_DEVICES=0 ./bandwidthTest
       tags:
         # use tags to get a GPU-enabled runner. This example will give
         # you a runner with the Power architecture and a CUDA capable GPU.
         - cuda
         - ppc64le
     ```
2. Use tags to get a CUDA-capable runner.

  - `cuda` will give you any runner with a CUDA capable GPU.
  - `ppc64le` will give you a runner with a CPU based on the POWER architecture.
  - `p100` will give you a runner with at least one Tesla P100 inside.
  - `x86_64` will give you a runner with an x86 ISA.

  Specify both the CPU architecture and the `cuda` tag to really be sure which
  runner will pick up your job.
